from __future__ import annotations

from abc import (
    ABC,
    abstractmethod,
)
from typing import Iterator


class ProxyScraper(ABC):

    @abstractmethod
    def __next__(self) -> ScrapedProxy:
        pass

    @abstractmethod
    def __iter__(self) -> Iterator[ScrapedProxy]:
        pass


class ScrapedProxy(ABC):

    def __init__(self):
        self._url = None
        self._port = None
